Source: libdbix-class-journal-perl
Maintainer: Debian Perl Group <pkg-perl-maintainers@lists.alioth.debian.org>
Uploaders: Mason James <mtj@kohaaloha.com>
Section: perl
Testsuite: autopkgtest-pkg-perl
Priority: optional
Build-Depends: debhelper-compat (= 13)
Build-Depends-Indep: libclass-c3-componentised-perl <!nocheck>,
                     libdbd-sqlite3-perl,
                     libdbix-class-perl <!nocheck>,
                     libmodule-install-perl,
                     libsql-translator-perl,
                     libtest-simple-perl <!nocheck>,
                     perl
Standards-Version: 4.6.2
Vcs-Browser: https://salsa.debian.org/perl-team/modules/packages/libdbix-class-journal-perl
Vcs-Git: https://salsa.debian.org/perl-team/modules/packages/libdbix-class-journal-perl.git
Homepage: https://metacpan.org/release/DBIx-Class-Journal
Rules-Requires-Root: no

Package: libdbix-class-journal-perl
Architecture: all
Depends: ${misc:Depends},
         ${perl:Depends},
         libclass-c3-componentised-perl,
         libdbix-class-perl
Description: auditing for tables managed by DBIx::Class
 DBIx::Class::Journal provides the basic utilities to write tests against
 DBIx::Class.
 .
 The purpose of this DBIx::Class component module is to create an
 audit-trail for all changes made to the data in your database (via a
 DBIx::Class schema). It creates *changesets* and assigns each
 create/update/delete operation an *id*. The creation and deletion date
 of each row is stored, as well as the historical contents of any row
 that gets changed.
 .
 All queries which need auditing must be called using "txn_do" in
 DBIx::Class::Schema, which is used to create changesets for each
 transaction.
 .
 To track who did which changes, the "user_id" (an integer) of the
 current user can be set, and a "session_id" can also be set; both are
 optional. To access the auditing schema to look at the auditdata or
 revert a change, use "$schema->_journal_schema".
